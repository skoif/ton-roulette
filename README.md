# ton-roulette

Ton roulette demo app

## Getting started

Настройка сервиса:
Данные кошелька указываются в файле config.ts

Все отсальное настраивается ENV'ами:

- PORT - порт веб-интерфейса
- NCOINS - количество монет для начала раунда
- COMMISSION - комиссия сервиса
- MIN_PAYMENT - минимальный взнос
- RPC_URL - Адрес RPC
- RPC_API_KEY - API ключ