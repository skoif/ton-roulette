const config: Config = {

  publicKey: new Uint8Array([
    178, 128, 218,  18, 168,  60, 233,  33,
    199,  52, 226, 211, 255,  33,  42, 159,
    246, 184, 181,   6,  10,  55,  26, 110,
    17, 119, 133, 160, 201, 173,  76,  48
  ]),
  secretKey: new Uint8Array([
    67, 248,  62,  13, 214,  72, 139, 206,  95, 140, 161,
    104, 123, 164, 217,  60, 220, 161,  39, 182,  88,  10,
    142, 125, 215, 130,  43, 134, 226, 150, 170, 238, 178,
    128, 218,  18, 168,  60, 233,  33, 199,  52, 226, 211,
    255,  33,  42, 159, 246, 184, 181,   6,  10,  55,  26,
    110,  17, 119, 133, 160, 201, 173,  76,  48
  ]),
  Ncoins: 0.5, //Количество TON, необходимое для начала раунда
  commission: 0.1, //Комиссия сервиса ( Например, 0.3 - это 30% )
  minimalPayment: 0.1, //Минимальный платёж, остальные отклоняются
  rpcUrl: 'https://testnet.toncenter.com/api/v2/jsonRPC',
  rpcApiKey: '',
  port: '8080', //Порт для JSON API

}

type Config = {
  port: string,
  Ncoins: number,
  commission: number,
  minimalPayment: number,
  rpcUrl: string,
  rpcApiKey: string,
  publicKey: Uint8Array,
  secretKey: Uint8Array,
}

config.port = process.env.PORT || config.port
config.Ncoins = parseFloat( process.env.NCOINS || '' ) || config.Ncoins
config.commission = parseFloat( process.env.COMMISSION || '' ) || config.commission
config.minimalPayment = parseFloat( process.env.MIN_PAYMENT || '' ) || config.minimalPayment
config.rpcUrl = process.env.RPC_URL || config.rpcUrl
config.rpcApiKey = process.env.RPC_API_KEY || config.rpcApiKey

export default config