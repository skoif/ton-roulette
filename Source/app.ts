import config from "../config";
import { Ton } from './Lib'
import Roulette from './Roulette'
import TonWeb from "tonweb";

import express from 'express'
const app = express()

const Startup = async ()=>{
  await Ton.init({
    rpcUrl: config.rpcUrl,
    rpcApiKey: config.rpcApiKey,
    publicKey: config.publicKey,
    secretKey: config.secretKey,
  })

  // const deploy = Ton.Wallet.deploy( config.secretKey )
  // const deployFee = await deploy.estimateFee()
  // const deploySended = await deploy.send() // deploy wallet contract to blockchain
  // const deployQuery = await deploy.getQuery();   // get deploy query Cell
  // console.log( { deploy, deployFee, deploySended, deployQuery } )

  console.log( 'ADDRESS: ' + await Ton.GetAddress() )
  console.log( 'BALANCE: ' + await Ton.GetBalanceHumanReadable() )

  app.get( '*', async (req,res)=>{
    res.json({
      address: await Ton.GetAddress(),
      roundTotal: Roulette.GetRoundTotal(),
      roundTotalHumanReadable: Roulette.GetRoundTotalHumanReadable(),
      minRoundTotal: TonWeb.utils.toNano( config.Ncoins ).toString(),
      minDeposit: TonWeb.utils.toNano( config.minimalPayment ).toString()
    })
  } )
  app.listen( config.port )

  await Roulette.Worker()
  setInterval( ()=>{ Roulette.Worker() }, 15000 )
}

Startup().catch( (err)=>{
  console.error( err )
  process.exit( 1 )
} )