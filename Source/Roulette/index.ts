import TonWeb from "tonweb";
import { WeightedRandom, Ton } from "../Lib";
import config from "../../config";

type RoundTransaction = {
  value: number,
  address: string,
}

export default class Roulette {
  static lastTransactionLt: string
  static currentRoundTransactions: RoundTransaction[] = []

  static async Worker(){
    if( !this.lastTransactionLt ){
      this.lastTransactionLt = ( await Ton.GetTransactions() )[0].transaction_id.lt
      return
    }

    let transactions = await Ton.GetValueTransactions( 100, this.lastTransactionLt )
    if( transactions.length > 0 ) {
      this.lastTransactionLt = transactions[0].transaction_id.lt

      transactions.forEach( ( transaction: any ) => {
        if( parseInt(transaction.in_msg.value) >= TonWeb.utils.toNano( config.minimalPayment ) ) {
          this.currentRoundTransactions.push({
            value: parseInt(transaction.in_msg.value),
            address: transaction.in_msg.source,
          })
        }
      })

      let roundTotal = this.GetRoundTotal()
      console.log( 'Current round total: ' + TonWeb.utils.fromNano( roundTotal ) )

      if( roundTotal >= TonWeb.utils.toNano( config.Ncoins ) ){
        let winnerId = WeightedRandom( this.currentRoundTransactions.map( (item)=>{ return item.value } ) )
        let winner = this.currentRoundTransactions[ winnerId ]
        let fee: number = ( await Ton.GetFees( winner.address, roundTotal ) ).total
        let winTotal = Math.round( roundTotal * ( 1 - config.commission ) - fee )
        console.log( 'WINNER: ' + winner.address + ' AMOUNT: ' + TonWeb.utils.fromNano( winTotal ) )
        this.currentRoundTransactions = []
        await Ton.SendTon( winTotal, winner.address )
      }
    }
  }

  static GetRoundTotal(): number {
    return this.currentRoundTransactions.reduce( (total,transaction)=>{ return total + transaction.value }, 0 )
  }

  static GetRoundTotalHumanReadable(): string {
    return TonWeb.utils.fromNano( this.GetRoundTotal() )
  }

}