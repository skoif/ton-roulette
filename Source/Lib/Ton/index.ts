import TonWeb from "tonweb"

type Config = {
  rpcUrl: string,
  rpcApiKey: string,
  publicKey: Uint8Array,
  secretKey: Uint8Array,
}

type Fees = {
  in_fwd_fee: number,
  storage_fee: number,
  gas_fee: number,
  fwd_fee: number,
  total: number,
}

export class Ton {
  private static rpcUrl: string;
  private static rpcApiKey: string;
  private static publicKey: Uint8Array;
  private static secretKey: Uint8Array;
  static TonWeb: any;
  static Rpc: any;
  static Wallet: any;
  static Address: any;

  static async init( config: Config ){
    try{
      this.TonWeb = TonWeb
      this.rpcUrl = config.rpcUrl
      this.rpcApiKey = config.rpcApiKey
      this.publicKey = config.publicKey
      this.secretKey = config.secretKey
      this.Rpc = new TonWeb( new TonWeb.HttpProvider(
        this.rpcUrl,
        {
          apiKey: this.rpcApiKey
        }
      ));
      this.Wallet = this.Rpc.wallet.create({
        publicKey: this.publicKey,
        wc: 0
      })
      this.Address = await this.Wallet.getAddress()
    }catch (e) {
      console.error( e )
      process.exit( 1 )
    }
  }

  static async GenerateKeypair(): Promise< { publicKey: Uint8Array, secretKey: Uint8Array } > {
    return TonWeb.utils.nacl.sign.keyPair()
  }

  static async GetAddress(): Promise< String > {
    return this.Address.toString( true, true, true, false, 0 )
  }

  static async GetFees( toAddress: string = 'EQCUOOo1zcJlRn3ukXVsY06WpLs5sXlV5qeT9wecMeftBIFL', amount: number = TonWeb.utils.toNano(0.1) ): Promise< Fees >  {
    let fees = ( await this.Wallet.methods.transfer({
      secretKey: this.secretKey,
      toAddress,
      amount,
      seqno: await this.Wallet.methods.seqno().call(),
      payload: null,
      sendMode: 3,
    }).estimateFee() ).source_fees
    delete fees['@type']

    fees.total = Object.keys( fees ).reduce( (total, key)=>{ return total + fees[ key ] }, 0 )

    return fees
  }

  static async GetBalance(): Promise< Number > {
    return parseInt( await this.Rpc.getBalance( this.Address ) )
  }

  static async GetBalanceHumanReadable(): Promise< String > {
    return TonWeb.utils.fromNano( await this.Rpc.getBalance( this.Address ) )
  }

  static async GetTransactions( limit: number = 100, to_lt: string | undefined = undefined, lt: string | undefined = undefined, hash: string | undefined = undefined ): Promise< any[] > {
    return await this.Rpc.getTransactions( this.Address, limit, lt, hash, to_lt )
  }

  static async GetValueTransactions( limit: number = 100, to_lt: string | undefined = undefined, lt: string | undefined = undefined, hash: string | undefined = undefined ): Promise< any[] > {
    return ( await this.GetTransactions( limit, to_lt, lt, hash ) )
      .filter( ( transaction ) => { return parseInt( transaction.in_msg.value ) > 0 } )
  }

  static async SendTon( amount: number = 0, address: string = '' ){
    if( amount < 1000 ){ amount = TonWeb.utils.toNano( amount ) }
    let seqno = await this.Wallet.methods.seqno().call()
    return await this.Wallet.methods.transfer({
      secretKey: this.secretKey,
      toAddress: address,
      amount,
      seqno,
      payload: null,
      sendMode: 3,
    }).send()
  }

}