export const WeightedRandom = ( weights: number[] = [] ): number => {

  let totalWeight: number = 0
  let i: number

  for (i = 0; i < weights.length; i++) {
    totalWeight += weights[i];
  }

  let random: number = Math.random() * totalWeight;

  for (i = 0; i < weights.length; i++) {
    if (random < weights[i]) {
      return i;
    }
    random -= weights[i];
  }

  return -1;

}